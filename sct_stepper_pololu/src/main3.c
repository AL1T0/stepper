/*
 By AL1T0

#include "main.h"
#include "board.h"

// Instancia de la máquina de estado
static Stepper statechart;

// Definiciones para configurar todas las entradas digitales usadas en la aplicación
==================[auxiliary functions definition]==========================
typedef struct {
	uint8_t pingrp;		 Pin group
	uint8_t pinnum;		 Pin number
	uint16_t modefunc;	 Pin mode and function for SCU
} PINMUX_GRP_T_AMG;

typedef struct {
	uint8_t port;
	uint8_t pin;
} io_port_t_amg;

void Chip_SCU_SetPinMuxing_AMG(const PINMUX_GRP_T_AMG *pinArray, uint32_t arrayLength)
{
	uint32_t ix;
	for (ix = 0; ix < arrayLength; ix++ ) {
		Chip_SCU_PinMuxSet(pinArray[ix].pingrp, pinArray[ix].pinnum, pinArray[ix].modefunc);
	}
}

static const io_port_t_amg gpioInputBits[] = {{0,4},{0,8},{0,9},{1,9},{3,0},{3,4},{5,6},{3,6}};

//Función para configurar las 4 teclas y 4 pines adicionales como entradas sin pull up, ni pull down, con buffer
void Board_Inputs_Init_AMG(void)
{
	PINMUX_GRP_T_AMG pin_config[] = {
			{1, 0, MD_PLN|MD_EZI|FUNC0},	 TEC1 -> P1_0
			{1, 1, MD_PLN|MD_EZI|FUNC0},	 TEC2 -> P1_1
			{1, 2, MD_PLN|MD_EZI|FUNC0},	 TEC3 -> P1_2
			{1, 6, MD_PLN|MD_EZI|FUNC0}, 	 TEC4 -> P1_6
			{6, 1, MD_PUP|MD_EZI|FUNC0},	 GPIO0 -> P6_1
			{6, 5, MD_PUP|MD_EZI|FUNC0},	 GPIO2 -> P6_5
			{6, 8, MD_PUP|MD_EZI|FUNC4},	 GPIO4 -> P6_8
			{6, 10, MD_PUP|MD_EZI|FUNC0}, 	 GPIO6 -> P6_10

	};

	Chip_SCU_SetPinMuxing_AMG(pin_config, (sizeof(pin_config) / sizeof(PINMUX_GRP_T_AMG)));

	for (uint8_t i = 0; i < (sizeof(gpioInputBits) / sizeof(io_port_t_amg)); i++) {
		Chip_GPIO_SetPinDIRInput(LPC_GPIO_PORT, gpioInputBits[i].port, gpioInputBits[i].pin);
	}
}
==================[End of auxiliary functions definition]==========================

// Inicialización de periféricos
static void initHardware(void)
{
	SystemCoreClockUpdate();
	// Inicialización del SysTick
	SysTick_Config(SystemCoreClock / 1000);

	//Inhabilito interrupciones temporalmente para la configuracion
	NVIC_DisableIRQ(PIN_INT0_IRQn);
	NVIC_DisableIRQ(PIN_INT1_IRQn);
	NVIC_DisableIRQ(PIN_INT2_IRQn);
	NVIC_DisableIRQ(PIN_INT3_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
	Chip_PININT_DeInit(LPC_GPIO_PIN_INT);

	//Inicializo los 6 leds, las teclas y las entradas
	Board_Init();
	Board_Inputs_Init_AMG();

	//Inicializo las 4 salidas del motor
	Chip_SCU_PinMux(6,4,MD_PUP|MD_EZI,FUNC0);
	Chip_SCU_PinMux(6,7,MD_PUP|MD_EZI,FUNC4);
	Chip_SCU_PinMux(6,9,MD_PUP|MD_EZI,FUNC0);
	Chip_SCU_PinMux(6,11,MD_PUP|MD_EZI,FUNC0);

	Chip_GPIO_SetDir(LPC_GPIO_PORT, 3,(1<<3)|(1<<5)|(1<<7),1);
	Chip_GPIO_SetDir(LPC_GPIO_PORT, 5,(1<<15),1);

	Chip_GPIO_ClearValue(LPC_GPIO_PORT, 3,(1<<3)|(1<<5)|(1<<7));
	Chip_GPIO_ClearValue(LPC_GPIO_PORT, 5,(1<<15));


	//Inicializo la UART para debug
	Board_Debug_Init();

	//Interrupción TEC_1 - evTec1 - Girar a la izquierda en manual - PININTCH0
	Chip_SCU_GPIOIntPinSel(0,0,4);

	//Interrupción TEC_2 - evTec2 - Girar a la derecha en manual - PININTCH1
	Chip_SCU_GPIOIntPinSel(1,0,8);

	//Interrupción TEC_4 - evTec4 - Alternar modo Manual/Automático - PININTCH2
	Chip_SCU_GPIOIntPinSel(2,1,9);
	
	//Interrupción GPIO0 - Entrada 1 encoder SP - PININTCH3
	Chip_SCU_GPIOIntPinSel(3,3,0);

	//Interrupción GPIO2 - Entrada 2 encoder SP - PININTCH4
	Chip_SCU_GPIOIntPinSel(4,3,4);

	//Interrupción GPIO4 - Entrada 1 encoder SP - PININTCH5
	Chip_SCU_GPIOIntPinSel(5,5,6);

	//Interrupción GPIO6 - Entrada 2 encoder SP - PININTCH6
	Chip_SCU_GPIOIntPinSel(6,3,6);

	//Habilito interrupciones de GPIO
	Chip_PININT_Init(LPC_GPIO_PIN_INT);

	//Configuro interrupcion por flanco
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH0 |
												 PININTCH1 |
												 PININTCH2 |
												 PININTCH3 |
												 PININTCH4 |
												 PININTCH5 |
												 PININTCH6 );

	//Configuro interrupcion por nivel
	Chip_PININT_SetPinModeLevel(LPC_GPIO_PIN_INT, PININTCH0 |
												  PININTCH1 |
												  PININTCH2 |
												  PININTCH3 |
												  PININTCH4 |
												  PININTCH5 |
												  PININTCH6 );

	//Habilito interrupcion por flanco/nivel
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT,   PININTCH0 |
												 PININTCH1 |
												 PININTCH2 |
												 PININTCH3 |
												 PININTCH4 |
												 PININTCH5 |
												 PININTCH6 );

	Chip_PININT_DisableIntLow(LPC_GPIO_PIN_INT,  PININTCH0 |
												 PININTCH1 |
												 PININTCH2 |
												 PININTCH3 |
												 PININTCH4 |
												 PININTCH5 |
												 PININTCH6 );

	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH0 |
												 PININTCH1 |
												 PININTCH2 |
												 PININTCH3 |
												 PININTCH4 |
												 PININTCH5 |
												 PININTCH6 );

	Chip_PININT_DisableIntHigh(LPC_GPIO_PIN_INT, PININTCH0 |
												 PININTCH1 |
												 PININTCH2 |
												 PININTCH3 |
												 PININTCH4 |
												 PININTCH5 |
												 PININTCH6 );

	//Limpiar interupciones pendientes
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
	NVIC_EnableIRQ(PIN_INT3_IRQn);
	NVIC_EnableIRQ(PIN_INT4_IRQn);
	NVIC_EnableIRQ(PIN_INT5_IRQn);
	NVIC_EnableIRQ(PIN_INT6_IRQn);

	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT3_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT4_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT5_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT6_IRQn);
}

void SystemInit(void)
{
#if defined(CORE_M3) || defined(CORE_M4)
	unsigned int *pSCB_VTOR = (unsigned int *) 0xE000ED08;

#if defined(__IAR_SYSTEMS_ICC__)
	extern void *__vector_table;

	*pSCB_VTOR = (unsigned int) &__vector_table;
#elif defined(__CODE_RED)
	extern void *g_pfnVectors;

	*pSCB_VTOR = (unsigned int) &g_pfnVectors;
#elif defined(__ARMCC_VERSION)
	extern void *__Vectors;

	*pSCB_VTOR = (unsigned int) &__Vectors;
#endif

#if defined(__FPU_PRESENT) && __FPU_PRESENT == 1
	fpuInit();
#endif

#if defined(NO_BOARD_LIB)
	 Chip specific SystemInit
	Chip_SystemInit();
#else
	 Board specific SystemInit
	//Board_SystemInit();
#endif

#endif  defined(CORE_M3) || defined(CORE_M4)
}

==================[external functions definition]==========================
* state machine user-defined external function (action)

void stepperIfaceModo_opLed(const Stepper* handle, const sc_boolean R, const sc_boolean G)
{
	//Alterno leds rojo (modo manual) y verde (modo automático)
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) R);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) G);
}

void stepperIface_opSec(const Stepper* handle, const sc_boolean Q1, const sc_boolean Q2, const sc_boolean Q3, const sc_boolean Q4)
{
	//Función para prender leds/salidas en la secuencia de control del motor
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
//	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) Q2);
//	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) Q3);
//	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) Q4);
//	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
}

void stepperIface_iRQOff(const Stepper* handle)
{
	//Inhabilito temporalmente las interrupciones de teclas
	NVIC_DisableIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_DisableIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_DisableIRQ(PIN_INT2_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
}
void stepperIface_iRQOn(const Stepper* handle)
{
	//Vuelvo a habilitar las interrupciones de teclas
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
}

void stepperIface_printPasos(const Stepper* handle, const sc_integer n)
{
	printf("Paso %d \n", n);
}



void GPIO0_IRQHandler(void){
	//Interrupción por acción de la Tecla 1
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH0);
	//Chip_PININT_ClearFallStates(LPC_GPIO_PIN_INT, PININTCH0);
	stepperIfaceManual_raise_evTec1On(&statechart);
	stepper_runCycle(&statechart);
	printf("evTec1_On\n");
}

void GPIO1_IRQHandler(void){
	//Interrupción por acción de la Tecla 2
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH1);
	//Chip_PININT_ClearFallStates(LPC_GPIO_PIN_INT, PININTCH0);
	stepperIfaceManual_raise_evTec2On(&statechart);
	stepper_runCycle(&statechart);
	printf("evTec2_On\n");

}

void GPIO2_IRQHandler(void){
	//Interrupción por acción de la Tecla 4
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH2);
	stepperIfaceModo_raise_evTec4(&statechart);
	stepper_runCycle(&statechart);
}

void GPIO3_IRQHandler(void){
	//SW 1 del encoder 1
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH3);
	printf("S11\n");
}

void GPIO4_IRQHandler(void){
	//SW 2 del encoder 1
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH4);
	printf("S21\n");
}

void GPIO5_IRQHandler(void){
	//SW 1 del encoder 2
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH5);
	printf("S12\n");
}

void GPIO6_IRQHandler(void){
	//SW 2 del encoder 2
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH6);
	printf("S22\n");
}

* SysTick interrupt handler
void SysTick_Handler(void)
{
	// Evento Tick
	stepperIface_raise_evTick(&statechart);

	// Actualizar la máquina de estados
	stepper_runCycle(&statechart);
}

int main(void){

	initHardware();

	// Inicializar y resetear la máquina de estados
	stepper_init(&statechart);
	stepper_enter(&statechart);

	while (1) {
		__WFI();  wait for interrupt
	}
}
*/
