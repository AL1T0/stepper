//// Trabajo práctico integrador de la aignatura Electrónica Digital II
//// * de la Universidad Nacional de San Martín (UNSAM)
//// * Control de posición de motor paso a paso con Driver A4988 (Pololu)
//// * Dos modos de funcionamiento que se alternan con la tecla 4 de la placa
//// * - Manual: con las teclas 1 y 2 se indica el sentido de giro del motor
//// * (TEC1=izq ; TEC2=der)
//// * - Automático: con un potenciómetro conectado en el pin ADC_1 se establece
//// * el Set Point al que se requiere llevar el motor. El potenciómetro que se
//// * encuentra conectado al ADC_2 se encuentra acoplado al eje del motor y
//// * devuelve la posición actual, estos valores se comparan y se trata
//// * de llevar al motor al SP
//
//#include "main.h"
////#include "board.h"
//#include "Stepper.h"
////#include "funciones.c"
//
//// Instancia de la máquina de estados
//static Stepper statechart;
//
//// Definiciones adicionales
//static _ADC_CLOCK_SETUP_T ADCSetup;
//uint32_t ADC_bitRate = 50;
//_ADC_RESOLUTION_T ADC_bitAccuracy = R10BITS;
//uint16_t dataADC_CH1;
//uint16_t PresentValue;
//uint16_t SetPoint;
//uint16_t nPasos;
//uint32_t ADC_Status;
//
//// Inicialización de periféricos
//static void initHardware(void)
//{
//	SystemCoreClockUpdate();
//
//	// Inicialización del SysTick con base de 1ms
//	SysTick_Conf(SystemCoreClock/1000);
//
//	//Inicializo los periféricos de la placa
//	GPIO_Init(_GPIO_PIN_INT);
//	Config_LEDS(SCU_MODE_DES | SCU_MODE_EZI);
//	Config_Botones(SCU_MODE_DES | SCU_MODE_EZI);
//
//	//Inicializo la UART para debug
//	UART_Init();
//
//	//Inicializo las 2 salidas para el control del motor
//	SCU_PinMux(6,7,SCU_MODE_DES|SCU_MODE_EZI|SCU_MODE_FUNC4); //Direccion (0:izq ; 1:der)
//	SCU_PinMux(6,8,SCU_MODE_DES|SCU_MODE_EZI|SCU_MODE_FUNC4); //Pulso de habilitación de paso
//	GPIO_SetPortDIROutput(_GPIO_PORT, 5,(1<<15)|(1<<16));
//	GPIO_ClearPin(_GPIO_PORT, 5,(1<<15)|(1<<16),1);
//
//	//Interrupción TEC_1 - evTec1 - Girar a la izquierda en manual - PININTCH0
//	SCU_GPIOIntPinSel(0,0,4);
//
//	//Interrupción TEC_2 - evTec2 - Girar a la derecha en manual - PININTCH1
//	SCU_GPIOIntPinSel(1,0,8);
//
//	//Interrupción TEC_4 - evTec4 - Alternar modo Manual/Automático - PININTCH2
//	SCU_GPIOIntPinSel(2,1,9);
//
//	//Habilito interrupciones de GPIO
//	PININT_Init(_GPIO_PIN_INT);
//
//	//Configuro interrupcion por flanco
//	PININT_Edge(_GPIO_PIN_INT,	PININTCH0 |
//								PININTCH1 |
//								PININTCH2 );
//
//	//Habilito interrupcion por flanco/nivel bajo
//	PININT_Low(_GPIO_PIN_INT,   PININTCH0 |
//								PININTCH1 |
//								PININTCH2 );
//
//	//Habilito interrupcion por flanco/nivel alto
//	PININT_High(_GPIO_PIN_INT,  PININTCH0 |
//								PININTCH1 );
//
//	//Configuración del ADC -  Canal 1
//	Chip_ADC_Init(LPC_ADC0, &ADCSetup);
//	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);
//	Chip_ADC_SetSampleRate(LPC_ADC0, &ADCSetup, ADC_bitRate);
//	Chip_ADC_SetResolution(LPC_ADC0, &ADCSetup, ADC_bitAccuracy);
//	Chip_ADC_SetBurstCmd(LPC_ADC0, DISABLE);
//
//	//Configuración del ADC -  Canal 2
//	Chip_ADC_Init(LPC_ADC1, &ADCSetup);
//	Chip_ADC_EnableChannel(LPC_ADC1, ADC_CH2, ENABLE);
//	Chip_ADC_SetSampleRate(LPC_ADC1, &ADCSetup, ADC_bitRate);
//	Chip_ADC_SetResolution(LPC_ADC1, &ADCSetup, ADC_bitAccuracy);
//	Chip_ADC_SetBurstCmd(LPC_ADC1, DISABLE);
//
//	//Habilitación de interupciones de teclas
//	NVIC_EnaIRQ(PIN_INT0_IRQn);
//	NVIC_EnaIRQ(PIN_INT1_IRQn);
//	NVIC_EnaIRQ(PIN_INT2_IRQn);
//
//	//Establezco prioridad de la interrupción por GPIO
//	NVIC_SetPri(PIN_INT0_IRQn,15);
//	NVIC_SetPri(PIN_INT1_IRQn,15);
//	NVIC_SetPri(PIN_INT2_IRQn,15);
//
//	//Clear interupciones pendientes
//	NVIC_ClrPendIRQ(PIN_INT0_IRQn);
//	NVIC_ClrPendIRQ(PIN_INT1_IRQn);
//	NVIC_ClrPendIRQ(PIN_INT2_IRQn);
//
//	//Habilitación ADC e inicio de una conversion
//	ADC_Canal(ADC0, CH1, 1);
//	ADC_Canal(ADC1, CH2, 1);
//	ADC_Habilitar(ADC0, START_NOW, TRIGGERMODE_RISING);
//	ADC_Habilitar(ADC1, START_NOW, TRIGGERMODE_RISING);
//}
//
////============================[Definición de funciones externas]=============================
////******[Acciones de la máquina de estados]******
//
//// Función para alternar entre los modos Manual/Automático
//void stepperIfaceModo_opLed(const Stepper* handle, const sc_boolean R, const sc_boolean G)
//{
//	//Alterno leds rojo (modo manual) y verde (modo automático)
//	// En Manual deshabilito el ADC
//	if (R) {
//		LED_ON(LEDR);
//		LED_OFF(LEDG);
//		ADC_Canal(ADC0, CH1, 0);
//		ADC_Canal(ADC1, CH2, 0);
//	}
//
//	// En Automatico habilito el ADC
//	if (G) {
//		LED_OFF(LEDR);
//		LED_ON(LEDG);
//		ADC_Canal(ADC0, CH1, 1);
//		ADC_Canal(ADC1, CH2, 1);
//	}
//}
//
//// Función para prender leds/salidas en la secuencia de control del motor
//void stepperIface_opStep(const Stepper* handle, const sc_boolean Dir, const sc_boolean Step)
//{
//	GPIO_EscribirPin(LPC_GPIO_PORT, 5, 16, (bool) Dir);
//	GPIO_EscribirPin(LPC_GPIO_PORT, 5, 15, (bool) Step);
//	if (Dir) {
//		LED_ON(LED1);
//	} else {
//		LED_OFF(LED1);
//	}
//
//	if (Step) {
//		LED_ON(LED2);
//	} else {
//		LED_OFF(LED2);
//	}
//
//}
//
//// Función para imprimir el paso actual del motor
//void stepperIface_printPasos(const Stepper* handle, const sc_integer n)
//{
//	printf("Paso %d \n", n);
////	Chip_ADC_SetStartMode(LPC_ADC1, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
//}
//
//// Función para leer el ADC
//void stepperIface_opADC(const Stepper* handle)
//{
//	ADC_Leer(ADC0, CH1, &dataADC_CH1);
//	ADC_Leer(ADC1, CH2, &PresentValue);
//	//printf("ADC 1: %d \nADC 2: %d \n", dataADC_CH1, PresentValue);
//	SetPoint=dataADC_CH1*589/1023;
//
//	//Comparo los valores de los dos canales del ADC
//	if (PresentValue <= 589) {
//	if (SetPoint>PresentValue && ((SetPoint-PresentValue) > 3)){  //SP>PV
//		stepperIfaceAuto_raise_evMayor(&statechart);
//	} else {
//		if (SetPoint<PresentValue  && ((PresentValue-SetPoint) > 3)) {  //SP<PV
//			stepperIfaceAuto_raise_evMenor(&statechart);
//		} else {
//			stepperIfaceAuto_raise_evIgual(&statechart);
//		}
//	}
//	stepper_runCycle(&statechart);
//	}
//
//	//Inicio nueva conversion del ADC
//	ADC_Habilitar(ADC0, START_NOW, TRIGGERMODE_RISING);
//	ADC_Habilitar(ADC1, START_NOW, TRIGGERMODE_RISING);
//}
//
////******[Handlers de interrupciones]******
//// Interrupción de la Tecla 1 de la placa
//void GPIO0_IRQHandler(void){
//	//Interrupción por acción de la Tecla 1
//	_Bool T1_Rise = (_GPIO_PIN_INT->RISE)&(1 << 0);
//	_Bool T1_Fall = (_GPIO_PIN_INT->FALL)&(1 << 0);
//	PININT_ClearInt(_GPIO_PIN_INT, PININTCH0);
//
//	if (T1_Rise) { // Verifico si la interrupción fue por flanco ascendente
//	stepperIfaceManual_raise_evTec1Off(&statechart);
//	}
//	else
//	{
//		if (T1_Fall) { 	// Verifico si la interrupción fue por flanco descendente
//		stepperIfaceManual_raise_evTec1On(&statechart);
//		//printf("evTec1_On\n");
//		}
//	}
//	stepper_runCycle(&statechart);
//}
//
//// Interrupción de la Tecla 2 de la placa
//void GPIO1_IRQHandler(void){
//	//Interrupción por acción de la Tecla 2
//	_Bool T2_Rise = (_GPIO_PIN_INT->RISE)&(1 << 1);
//	_Bool T2_Fall = (_GPIO_PIN_INT->FALL)&(1 << 1);
//	PININT_ClearInt(_GPIO_PIN_INT, PININTCH1);
//
//	if (T2_Rise) { // Verifico si la interrupción fue por flanco ascendente
//	stepperIfaceManual_raise_evTec2Off(&statechart);
//	//printf("evTec2_Off\n");
//	} else {
//		if (T2_Fall) { 	// Verifico si la interrupción fue por flanco descendente
//		stepperIfaceManual_raise_evTec2On(&statechart);
//		//printf("evTec2_On\n");
//		}
//	}
//	stepper_runCycle(&statechart);
//}
//
//// Interrupción de la Tecla 3 de la placa
//void GPIO2_IRQHandler(void){
//	//Interrupción por acción de la Tecla 4
//	PININT_ClearInt(_GPIO_PIN_INT, PININTCH2);
//	//printf("evTec4_On\n");
//	stepperIfaceModo_raise_evTec4(&statechart);
//	stepper_runCycle(&statechart);
//}
//
//// Interrupción del SysTick
//void SysTick_Handler(void)
//{
//	// Evento Tick
//	stepperIface_raise_evTick(&statechart);
//
//	// Actualizar la máquina de estados
//	stepper_runCycle(&statechart);
//}
//
////******[Rutina principal]******
//int main(void){
//
//	initHardware();
//
//	// Inicializar y resetear la máquina de estados
//	stepper_init(&statechart);
//	stepper_enter(&statechart);
//
//	// Leeer la posici[on inicial del motor y convertirlo a pasos
//	while (ADC_Estado(ADC1, CH2)==0){}
//	ADC_Leer(ADC1, CH2, &PresentValue);
//	nPasos=PresentValue*310/1023;
//	stepperIface_set_nPasos(&statechart, nPasos);
//	stepper_runCycle(&statechart);
//
//	//Evento de inicio de la máquina de estados
//	stepperIface_raise_evInit(&statechart);
//
//	// Actualizar la máquina de estados
//	stepper_runCycle(&statechart);
//
//	while (1) {
//		__WFI();  //wait for interrupt
//	}
//}
//
//void SystemInit(void)
//{
//#if defined(CORE_M3) || defined(CORE_M4)
//	unsigned int *pSCB_VTOR = (unsigned int *) 0xE000ED08;
//
//#if defined(__IAR_SYSTEMS_ICC__)
//	extern void *__vector_table;
//
//	*pSCB_VTOR = (unsigned int) &__vector_table;
//#elif defined(__CODE_RED)
//	extern void *g_pfnVectors;
//
//	*pSCB_VTOR = (unsigned int) &g_pfnVectors;
//#elif defined(__ARMCC_VERSION)
//	extern void *__Vectors;
//
//	*pSCB_VTOR = (unsigned int) &__Vectors;
//#endif
//
//#if defined(__FPU_PRESENT) && __FPU_PRESENT == 1
//	fpuInit();
//#endif
//
//#if defined(NO_BOARD_LIB)
//	 Chip specific SystemInit
//	Chip_SystemInit();
//#else
//	//Board specific SystemInit
//	//Board_SystemInit();
//#endif
//
//#endif  defined(CORE_M3) || defined(CORE_M4)
//}
