/*
 Trabajo práctico integrador de la aignatura Electrónica Digital II
 * de la Universidad Nacional de San Martín (UNSAM)
 * Control de posición de motor paso a paso
 * Driver A

#include "main.h"
#include "board.h"

// Instancia de la máquina de estado
static Stepper statechart;

// Definiciones adicionales
bool fGirando;
static ADC_CLOCK_SETUP_T ADCSetup;
uint32_t _bitRate = 10;
uint32_t TMR0_CNT = 0;
uint32_t timerFreq;
uint8_t dataADC_CH1;
uint8_t dataADC_CH2;
ADC_RESOLUTION_T _bitAccuracy = ADC_8BITS;
#define TICKRATE_HZ 10

// Inicialización de periféricos
static void initHardware(void)
{
	//Inhabilito interrupciones temporalmente para la configuracion
	NVIC_DisableIRQ(PIN_INT0_IRQn);
	NVIC_DisableIRQ(PIN_INT1_IRQn);
	NVIC_DisableIRQ(PIN_INT2_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
	Chip_PININT_DeInit(LPC_GPIO_PIN_INT);
	NVIC_DisableIRQ(TIMER0_IRQn);

	SystemCoreClockUpdate();

	// Inicialización del SysTick con base de 1us
	SysTick_Config(SystemCoreClock/1000);
	//printf("%d\n", SystemCoreClock);

	//Inicializo los periféricos de la placa
	Board_Init();

	//Inicializo la UART para debug
	Board_Debug_Init();

	//Inicializo las 2 salidas para el control del motor
	Chip_SCU_PinMux(6,7,MD_PLN|MD_EZI,FUNC4); //Direccion (0:izq ; 1:der)
	Chip_SCU_PinMux(6,8,MD_PLN|MD_EZI,FUNC4); //Pulso de habilitación de paso

//	Chip_GPIO_SetDir(LPC_GPIO_PORT, 3,(1<<3)|(1<<5)|(1<<7),1);
	Chip_GPIO_SetDir(LPC_GPIO_PORT, 5,(1<<15)|(1<<16),1);

//	Chip_GPIO_ClearValue(LPC_GPIO_PORT, 3,(1<<3)|(1<<5)|(1<<7));
	Chip_GPIO_ClearValue(LPC_GPIO_PORT, 5,(1<<15)|(1<<16));

	//Interrupción TEC_1 - evTec1 - Girar a la izquierda en manual - PININTCH0
	Chip_SCU_GPIOIntPinSel(0,0,4);

	//Interrupción TEC_2 - evTec2 - Girar a la derecha en manual - PININTCH1
	Chip_SCU_GPIOIntPinSel(1,0,8);

	//Interrupción TEC_4 - evTec4 - Alternar modo Manual/Automático - PININTCH2
	Chip_SCU_GPIOIntPinSel(2,1,9);

	//Habilito interrupciones de GPIO
	Chip_PININT_Init(LPC_GPIO_PIN_INT);

	//Configuro interrupcion por flanco
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH0 |
												 PININTCH1 |
												 PININTCH2 );

	//Habilito interrupcion por flanco/nivel bajo
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT,   PININTCH0 |
												 PININTCH1 |
												 PININTCH2 );

	//Habilito interrupcion por flanco/nivel alto
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT,  PININTCH0 |
												 PININTCH1 );
	//Inicializacion del TIMER 0
	Chip_TIMER_Init(LPC_TIMER0);
	Chip_RGU_TriggerReset(RGU_TIMER0_RST);
	while (Chip_RGU_InReset(RGU_TIMER0_RST)) {}

	// TIMER0 peripheral clock rate
	timerFreq = Chip_Clock_GetRate(CLK_MX_TIMER0);
//	printf("%d\n", timerFreq);

		 TIMER0 setup for match and interrupt at TICKRATE_HZ
	Chip_TIMER_PrescaleSet(LPC_TIMER0, 10);
	Chip_TIMER_Reset(LPC_TIMER0);
	Chip_TIMER_MatchEnableInt(LPC_TIMER0, 1);
	Chip_TIMER_SetMatch(LPC_TIMER0, 1, (timerFreq/TICKRATE_HZ));
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER0, 1);

	//Configuración del ADC -  Canal 1
	Chip_ADC_Init(LPC_ADC0, &ADCSetup);
	Chip_ADC_EnableChannel(LPC_ADC0, ADC_CH1, ENABLE);
	Chip_ADC_SetSampleRate(LPC_ADC0, &ADCSetup, _bitRate);
	Chip_ADC_SetResolution(LPC_ADC0, &ADCSetup, _bitAccuracy);
	Chip_ADC_SetBurstCmd(LPC_ADC0, DISABLE);

	//Configuración del ADC -  Canal 2
	Chip_ADC_Init(LPC_ADC1, &ADCSetup);
	Chip_ADC_EnableChannel(LPC_ADC1, ADC_CH2, ENABLE);
	Chip_ADC_SetSampleRate(LPC_ADC1, &ADCSetup, _bitRate);
	Chip_ADC_SetResolution(LPC_ADC1, &ADCSetup, _bitAccuracy);
	Chip_ADC_SetBurstCmd(LPC_ADC1, DISABLE);

	//Limpiar interupciones pendientes
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);

	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);

	//Habilito ADC y lanzo una conversion por primera vez
	Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, ENABLE);
	Chip_ADC_Int_SetChannelCmd(LPC_ADC1, ADC_CH2, ENABLE);
	Chip_ADC_SetStartMode(LPC_ADC0, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
	Chip_ADC_SetStartMode(LPC_ADC1, ADC_START_NOW, ADC_TRIGGERMODE_RISING);

	//Habilito interrupcion del TIMER 0
	Chip_TIMER_Enable(LPC_TIMER0);
	NVIC_EnableIRQ(TIMER0_IRQn);
	NVIC_ClearPendingIRQ(TIMER0_IRQn);
}

void SystemInit(void)
{
#if defined(CORE_M3) || defined(CORE_M4)
	unsigned int *pSCB_VTOR = (unsigned int *) 0xE000ED08;

#if defined(__IAR_SYSTEMS_ICC__)
	extern void *__vector_table;

	*pSCB_VTOR = (unsigned int) &__vector_table;
#elif defined(__CODE_RED)
	extern void *g_pfnVectors;

	*pSCB_VTOR = (unsigned int) &g_pfnVectors;
#elif defined(__ARMCC_VERSION)
	extern void *__Vectors;

	*pSCB_VTOR = (unsigned int) &__Vectors;
#endif

#if defined(__FPU_PRESENT) && __FPU_PRESENT == 1
	fpuInit();
#endif

#if defined(NO_BOARD_LIB)
	 Chip specific SystemInit
	Chip_SystemInit();
#else
	 Board specific SystemInit
	//Board_SystemInit();
#endif

#endif  defined(CORE_M3) || defined(CORE_M4)
}

==================[external functions definition]==========================
* state machine user-defined external function (action)

void stepperIfaceModo_opLed(const Stepper* handle, const sc_boolean R, const sc_boolean G)
{
	//Alterno leds rojo (modo manual) y verde (modo automático)
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) R);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) G);

	// Si estoy en Manual deshabilito la interrupcion del TIMER 0
	if (R) {
		Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, DISABLE);
		Chip_ADC_Int_SetChannelCmd(LPC_ADC1, ADC_CH2, DISABLE);
		Chip_TIMER_ClearMatch(LPC_TIMER0, 0);
		//Chip_TIMER_Disable(LPC_TIMER0);
		LPC_TIMER0->IR = 1;
		NVIC_DisableIRQ(TIMER0_IRQn);
	}

	//Si estoy en Automatico habilito la interrupcion del TIMER 0
	if (G) {
		Chip_ADC_Int_SetChannelCmd(LPC_ADC0, ADC_CH1, ENABLE);
		Chip_ADC_Int_SetChannelCmd(LPC_ADC1, ADC_CH2, ENABLE);
		Chip_TIMER_Enable(LPC_TIMER0);
		NVIC_EnableIRQ(TIMER0_IRQn);
	}
}

void stepperIface_opStep(const Stepper* handle, const sc_boolean Dir, const sc_boolean Step)
{
	//Función para prender leds/salidas en la secuencia de control del motor
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 16, (bool) Dir);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) Dir);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 15, (bool) Step);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) Step);
}

void stepperIface_opGirando(const Stepper* handle, const sc_boolean f)
{
	fGirando=f;
}

void stepperIface_iRQOff(const Stepper* handle)
{
	//Inhabilito temporalmente las interrupciones de teclas
	NVIC_DisableIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_DisableIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_DisableIRQ(PIN_INT2_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
}
void stepperIface_iRQOn(const Stepper* handle)
{
	//Vuelvo a habilitar las interrupciones de teclas
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
}

void stepperIface_printPasos(const Stepper* handle, const sc_integer n)
{
	printf("Paso %d \n", n);
}

void GPIO0_IRQHandler(void){
	//Interrupción por acción de la Tecla 1
	_Bool T1_Rise = (LPC_GPIO_PIN_INT->RISE)&(1 << 0);
	_Bool T1_Fall = (LPC_GPIO_PIN_INT->FALL)&(1 << 0);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH0);

	if (T1_Rise) { // Verifico si la interrupción fue por flanco ascendente
	stepperIfaceManual_raise_evTec1Off(&statechart);
	//stepper_runCycle(&statechart);
	printf("evTec1_Off\n");
	}
	else
	{
		if (T1_Fall) { 	// Verifico si la interrupción fue por flanco descendente
		stepperIfaceManual_raise_evTec1On(&statechart);
		printf("evTec1_On\n");
		}
	}
	stepper_runCycle(&statechart);
}

void GPIO1_IRQHandler(void){
	//Interrupción por acción de la Tecla 2
	_Bool T2_Rise = (LPC_GPIO_PIN_INT->RISE)&(1 << 1);
	_Bool T2_Fall = (LPC_GPIO_PIN_INT->FALL)&(1 << 1);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH1);

	if (T2_Rise) { // Verifico si la interrupción fue por flanco ascendente
	stepperIfaceManual_raise_evTec2Off(&statechart);
	//stepper_runCycle(&statechart);
	printf("evTec2_Off\n");
	} else {
		if (T2_Fall) { 	// Verifico si la interrupción fue por flanco descendente
		stepperIfaceManual_raise_evTec2On(&statechart);
		//stepper_runCycle(&statechart);
		printf("evTec2_On\n");
		}
	}
	stepper_runCycle(&statechart);
}

void GPIO2_IRQHandler(void){
	//Interrupción por acción de la Tecla 4
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH2);
	printf("evTec4_On\n");
	stepperIfaceModo_raise_evTec4(&statechart);
	stepper_runCycle(&statechart);
}

void TIMER0_IRQHandler(void)
{
	//Inhabilito interrupcion del TIMER 0
//	NVIC_DisableIRQ(TIMER0_IRQn);
//	Chip_TIMER_Disable(LPC_TIMER0);
//	Chip_TIMER_ClearMatch(LPC_TIMER0, 0);
	LPC_TIMER0->IR = 1;
	Chip_TIMER_PrescaleSet(LPC_TIMER0, 10);

	if (TMR0_CNT<=5000000) {
		TMR0_CNT++;
	} else {
	TMR0_CNT=0;
	//Leo los datos del ADC
	Chip_ADC_ReadByte(LPC_ADC0, ADC_CH1, &dataADC_CH1);
	Chip_ADC_ReadByte(LPC_ADC1, ADC_CH2, &dataADC_CH2);
	printf("ADC_1 %d \nADC_2 %d \n", dataADC_CH1, dataADC_CH2);

	//Comparo los valores de los dos canales del ADC
	if (dataADC_CH1==dataADC_CH2){
		stepperIfaceAuto_raise_evIgual(&statechart);
	} else {
		if (dataADC_CH1<dataADC_CH2) {
			stepperIfaceAuto_raise_evMenor(&statechart);
		} else {
			stepperIfaceAuto_raise_evMayor(&statechart);
		}
	}
	stepper_runCycle(&statechart);
	//Inicio nueva conversion del ADC
	Chip_ADC_SetStartMode(LPC_ADC0, ADC_START_NOW, ADC_TRIGGERMODE_RISING);
	Chip_ADC_SetStartMode(LPC_ADC1, ADC_START_NOW, ADC_TRIGGERMODE_RISING);

//	}

	//Habilito interrupcion del TIMER 0
//	Chip_TIMER_Enable(LPC_TIMER0);
//	NVIC_EnableIRQ(TIMER0_IRQn);
}

* SysTick interrupt handler
void SysTick_Handler(void)
{
	// Evento Tick
	stepperIface_raise_evTick(&statechart);

	// Actualizar la máquina de estados
	stepper_runCycle(&statechart);
}

int main(void){

	initHardware();

	// Inicializar y resetear la máquina de estados
	stepper_init(&statechart);
	stepper_enter(&statechart);

	while (1) {
		__WFI();  wait for interrupt
	}
}
*/
