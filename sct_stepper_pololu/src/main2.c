/*
 By AL1T0

#include "main.h"
#include "board.h"

// Instancia de la máquina de estado
static Stepper statechart;

// Inicialización de periféricos
static void initHardware(void)
{
	SystemCoreClockUpdate();
	// Inicialización del SysTick
	SysTick_Config(SystemCoreClock / 1000);

	//Inhebilito interrupciones temporalmente para la configuracion
	NVIC_DisableIRQ(PIN_INT0_IRQn);
	NVIC_DisableIRQ(PIN_INT1_IRQn);
	NVIC_DisableIRQ(PIN_INT2_IRQn);
	NVIC_DisableIRQ(PIN_INT3_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);

	//Inicializo los 6 leds y las teclas
	Board_Init();

	//Inicializo la UART para debug
	Board_Debug_Init();

	//Interrupción TEC_1 - evTec1 - Girar a la izquierda en manual
	Chip_SCU_GPIOIntPinSel(0,0,4);

	//Interrupción TEC_2 - evTec2 - Girar a la derecha en manual
	Chip_SCU_GPIOIntPinSel(1,0,8);

	//Interrupción TEC_4 - evTec4 - Alternar modo Manual/Automático
	Chip_SCU_GPIOIntPinSel(2,1,9);
	
	//Interrupción X
	//Chip_SCU_GPIOIntPinSel(3,1,9);

	//Habilito interrupciones de GPIO
	Chip_PININT_Init(LPC_GPIO_PIN_INT);

	//Configuro interrupcion por flanco
	Chip_PININT_SetPinModeEdge(LPC_GPIO_PIN_INT, PININTCH0 | PININTCH1 | PININTCH2);

	//Configuro interrupcion por nivel
	Chip_PININT_SetPinModeLevel(LPC_GPIO_PIN_INT, PININTCH0 | PININTCH1);

	//Habilito interrupcion por flanco/nivel alto
	Chip_PININT_EnableIntHigh(LPC_GPIO_PIN_INT, PININTCH0 | PININTCH1 | PININTCH2);

	//Habilito interrupcion por flanco/nivel bajo
	Chip_PININT_EnableIntLow(LPC_GPIO_PIN_INT, PININTCH0 | PININTCH1);

	//Limpiar inteerupciones pendientes
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);

	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
}

void SystemInit(void)
{
#if defined(CORE_M3) || defined(CORE_M4)
	unsigned int *pSCB_VTOR = (unsigned int *) 0xE000ED08;

#if defined(__IAR_SYSTEMS_ICC__)
	extern void *__vector_table;

	*pSCB_VTOR = (unsigned int) &__vector_table;
#elif defined(__CODE_RED)
	extern void *g_pfnVectors;

	*pSCB_VTOR = (unsigned int) &g_pfnVectors;
#elif defined(__ARMCC_VERSION)
	extern void *__Vectors;

	*pSCB_VTOR = (unsigned int) &__Vectors;
#endif

#if defined(__FPU_PRESENT) && __FPU_PRESENT == 1
	fpuInit();
#endif

#if defined(NO_BOARD_LIB)
	 Chip specific SystemInit
	Chip_SystemInit();
#else
	 Board specific SystemInit
	//Board_SystemInit();
#endif

#endif  defined(CORE_M3) || defined(CORE_M4)
}

==================[external functions definition]==========================
* state machine user-defined external function (action)

void stepperIfaceModo_opLed(const Stepper* handle, const sc_boolean R, const sc_boolean G)
{
	//Alterno leds rojo (modo manual) y verde (modo automático)
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 0, (bool) R);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 1, (bool) G);
}

void stepperIface_opSec(const Stepper* handle, const sc_boolean Q1, const sc_boolean Q2, const sc_boolean Q3, const sc_boolean Q4)
{
	//Función para prender leds/salidas en la secuencia de control del motor
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 0, 14, (bool) Q2);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 11, (bool) Q3);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 1, 12, (bool) Q4);
	Chip_GPIO_SetPinState(LPC_GPIO_PORT, 5, 2, (bool) Q1);
}

void stepperIface_iRQOff(const Stepper* handle)
{
	//Inhabilito temporalmente las interrupciones de teclas
	NVIC_DisableIRQ(PIN_INT0_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT0_IRQn);
	NVIC_DisableIRQ(PIN_INT1_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT1_IRQn);
	NVIC_DisableIRQ(PIN_INT2_IRQn);
	NVIC_ClearPendingIRQ(PIN_INT2_IRQn);
}
void stepperIface_iRQOn(const Stepper* handle)
{
	//Vuelvo a habilitar las interrupciones de teclas
	NVIC_EnableIRQ(PIN_INT0_IRQn);
	NVIC_EnableIRQ(PIN_INT1_IRQn);
	NVIC_EnableIRQ(PIN_INT2_IRQn);
}

void stepperIface_printPasos(const Stepper* handle, const sc_integer n)
{
	printf("Paso %d \n", n);
}

void GPIO0_IRQHandler(void){
	//Interrupción por acción de la Tecla 1
	_Bool T1_Rise = (LPC_GPIO_PIN_INT->RISE)&(1 << 0);
	_Bool T1_Fall = (LPC_GPIO_PIN_INT->FALL)&(1 << 0);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH0);
	
	if (!T1_Fall) { // Verifico si la interrupción fue por flanco ascendente
	stepperIfaceManual_raise_evTec1Off(&statechart);
	stepper_runCycle(&statechart);
	printf("evTec1_Off\n");
	}
	else
	{
		if (!T1_Rise) { 	// Verifico si la interrupción fue por flanco descendente
		stepperIfaceManual_raise_evTec1On(&statechart);
		stepper_runCycle(&statechart);
		printf("evTec1_On\n");
		}
	}
}

void GPIO1_IRQHandler(void){
	//Interrupción por acción de la Tecla 2
	_Bool T2_Rise = (LPC_GPIO_PIN_INT->RISE)&(1 << 1);
	_Bool T2_Fall = (LPC_GPIO_PIN_INT->FALL)&(1 << 1);
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH1);
	
	if (!T2_Fall) { // Verifico si la interrupción fue por flanco ascendente
	stepperIfaceManual_raise_evTec1Off(&statechart);
	stepper_runCycle(&statechart);
	printf("evTec2_Off\n");
	}
	else
	{
		if (!T2_Rise) { 	// Verifico si la interrupción fue por flanco descendente
		stepperIfaceManual_raise_evTec2On(&statechart);
		stepper_runCycle(&statechart);
		printf("evTec2_On\n");
		}
	}
}

void GPIO2_IRQHandler(void){
	//Interrupción por acción de la Tecla 4
	Chip_PININT_ClearIntStatus(LPC_GPIO_PIN_INT, PININTCH2);
	stepperIfaceModo_raise_evTec4(&statechart);
	stepper_runCycle(&statechart);
}

//void GPIO3_IRQHandler(void){
	//Reserva
//}


* SysTick interrupt handler
void SysTick_Handler(void)
{
	// Evento Tick
	stepperIface_raise_evTick(&statechart);

	// Actualizar la máquina de estados
	stepper_runCycle(&statechart);
}

int main(void){

	initHardware();

	// Inicializar y resetear la máquina de estados
	stepper_init(&statechart);
	stepper_enter(&statechart);

	while (1) {
		__WFI();  wait for interrupt
	}
}
*/
