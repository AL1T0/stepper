//#define	NO_LIBS

#include "board.h"
#include <stdbool.h>
#define  SYSTEM_BAUD_RATE 115200
#define CIAA_BOARD_UART LPC_USART2

enum LEDS {LED1, LED2, LED3, LEDR, LEDG, LEDB};

/************************************************************************************
 *	GPIO																			*
 ************************************************************************************/

#define _GPIO_PORT_BASE			0x400F4000	// direccion base del registro (pag. 455 / tabla 245)
#define _GPIO_PORT				((_GPIO_T *)	_GPIO_PORT_BASE)

#define GPIO_PORT0_B_OFFSET		0x0000		// tabla 259
#define GPIO_PORT1_B_OFFSET		0x0020		// tabla 259
#define GPIO_PORT0_DIR_OFFSET	0x2000		// tabla 261
#define GPIO_PORT1_DIR_OFFSET	0x2004		// tabla 261
#define GPIO_PORT0_SET_OFFSET	0x2200		// tabla 265
#define GPIO_PORT1_SET_OFFSET	0x2204		// tabla 265
#define GPIO_PORT0_CLR_OFFSET	0x2280		// tabla 266
#define GPIO_PORT1_CLR_OFFSET	0x2284		// tabla 266

// GPIO Register
typedef struct {				// Estructura para GPIO
	unsigned char B[128][32];	// Offset 0x0000: Byte pin registers ports 0 to n; pins PIOn_0 to PIOn_31 */
	int W[32][32];				// Offset 0x1000: Word pin registers port 0 to n
	int DIR[32];				// Offset 0x2000: Direction registers port n
	int MASK[32];				// Offset 0x2080: Mask register port n
	int PIN[32];				// Offset 0x2100: Portpin register port n
	int MPIN[32];				// Offset 0x2180: Masked port register port n
	int SET[32];				// Offset 0x2200: Write: Set register for port n Read: output bits for port n
	int CLR[32];				// Offset 0x2280: Clear port n
	int NOT[32];				// Offset 0x2300: Toggle port n
} _GPIO_T;

/************************************************************************************
 *	GPIO PIN INTERRUPT																*
 ************************************************************************************/
#define _PIN_INT_BASE		0x40087000		// Tabla 242, pag. 453
#define _GPIO_PIN_INT		((PIN_INT_T *) _PIN_INT_BASE)

#define PININTCH0         (1 << 0)
#define PININTCH1         (1 << 1)
#define PININTCH2         (1 << 2)
#define PININTCH3         (1 << 3)
#define PININTCH4         (1 << 4)
#define PININTCH5         (1 << 5)
#define PININTCH6         (1 << 6)
#define PININTCH7         (1 << 7)
#define PININTCH(ch)      (1 << (ch))

// Pin Interrupt and Pattern Match register block structure
typedef struct {			
	int ISEL;				// Pin Interrupt Mode register
	int IENR;				// Pin Interrupt Enable (Rising) register
	int SIENR;				// Set Pin Interrupt Enable (Rising) register
	int CIENR;				// Clear Pin Interrupt Enable (Rising) register
	int IENF;				// Pin Interrupt Enable Falling Edge / Active Level register
	int SIENF;				// Set Pin Interrupt Enable Falling Edge / Active Level register
	int CIENF;				// Clear Pin Interrupt Enable Falling Edge / Active Level address
	int RISE;				// Pin Interrupt Rising Edge register
	int FALL;				// Pin Interrupt Falling Edge register
	int IST;				// Pin Interrupt Status register
} PIN_INT_T;

/************************************************************************************
 *	GPIO PIN GROUP INTERRUPT												*
 ************************************************************************************/
#define _GRP0_INT_BASE		0x40088000	// direccion base del registro
#define _GPIO_GRP0_INT		((GRP0_INT_T *) _GRP0_INT_BASE)

#define GRP_COMB_OR			(0 << 1)
#define GRP_COMB_AND		(1 << 1)
#define GRP_TRIG_EDGE		(0 << 2)
#define GRP_TRIG_LEVEL		(1 << 2)

// Estructura
typedef struct {			
	int CTRL;				//Table 256. GPIO grouped interrupt control register
	int RESERVED0[7];
	int PORT_POL[8];		//Table 257. GPIO grouped interrupt port polarity registers
	int PORT_ENA[8];		//Table 258. GPIO grouped interrupt port n enable registers
} GRP0_INT_T;
	
/************************************************************************************
 *	System Control Unit (SCU)														*
 ************************************************************************************/

#define _SCU_BASE			0x40086000
#define _SCU				((SCU_T *) 	_SCU_BASE)

// Offset de los registros de configuracion para los pines (pag. 405)
// Leds		
#define	SFSP2_0				0x100	// pin P2_0  -> LED0_R (Puerto 5, Pin 0)
#define	SFSP2_1				0x104	// pin P2_1  -> LED0_G (Puerto 5, Pin 1)
#define	SFSP2_2				0x108	// pin P2_2  -> LED0_B (Puerto 5, Pin 2)
#define	SFSP2_10			0x128	// pin P2_10 -> LED1   (Puerto 0, Pin 14)
#define	SFSP2_11			0x12C	// pin P2_11 -> LED2   (Puerto 1, Pin 11)
#define	SFSP2_12			0x130	// pin P2_12 -> LED3   (Puerto 1, Pin 12)

// Funcion y modo (pag. 413)
#define SCU_MODE_EPD		(0 << 3)		// habilita la resistencia de pull-down (deshabilita con 0)
#define SCU_MODE_EPUN		(0 << 4)		// habilita la resistencia de pull-up (deshabilita con 1)
#define SCU_MODE_DES		(1 << 4)		// deshabilita las resistencias de pull-down y pull-up
#define SCU_MODE_EHZ		(1 << 5)		// 1 Rapido (ruido medio con alta velocidad)
											// 0 Lento (ruido bajo con velocidad media)
#define SCU_MODE_EZI		(1 << 6)		// habilita buffer de entrada (deshabilita con 0)
#define SCU_MODE_ZIF		(1 << 7)		// deshabilita el filtro anti glitch de entrada (habilita con 1)

#define SCU_MODE_FUNC0		0x0				// seleccion de la funcion 0 del pin
#define SCU_MODE_FUNC1		0x1				// seleccion de la funcion 1 del pin
#define SCU_MODE_FUNC2		0x2				// seleccion de la funcion 2 del pin
#define SCU_MODE_FUNC3		0x3				// seleccion de la funcion 3 del pin
#define SCU_MODE_FUNC4		0x4				// seleccion de la funcion 4 del pin
#define SCU_MODE_FUNC5		0x5				// seleccion de la funcion 5 del pin
#define SCU_MODE_FUNC6		0x6				// seleccion de la funcion 5 del pin
#define SCU_MODE_FUNC7		0x7				// seleccion de la funcion 5 del pin

// System Control Unit Register
typedef struct {
	int  SFSP[16][32];		// Los pines digitales estan divididos en 16 grupos (P0-P9 y PA-PF)
	int  RESERVED0[256];
	int  SFSCLK[4];			// Pin configuration register for pins CLK0-3
	int  RESERVED16[28];
	int  SFSUSB;			// Pin configuration register for USB
	int  SFSI2C0;			// Pin configuration register for I2C0-bus pins
	int  ENAIO[3];			// Analog function select registers
	int  RESERVED17[27];
	int  EMCDELAYCLK;		// EMC clock delay register
	int  RESERVED18[63];
	int  PINTSEL[2];		// Pin interrupt select register for pin int 0 to 3 index 0, 4 to 7 index 1
} SCU_T;

/************************************************************************************
 *	SysTick																			*
 ************************************************************************************/
#define _SysTick_BASE				0xE000E010	// Systick Base Address
#define _SysTick					((SysTick_T *) 	_SysTick_BASE )

// SysTick CTRL: Mascaras (ARM pag. B3-746)
#define SysTick_CTRL_COUNTFLAG	(1 << 16)	// SysTick CTRL: COUNTFLAG Mask
#define SysTick_CTRL_CLKSOURCE	(1 << 2)	// SysTick CTRL: CLKSOURCE Mask
#define SysTick_CTRL_TICKINT	(1 << 1)	// SysTick CTRL: TICKINT Mask
#define SysTick_CTRL_ENABLE		(1 << 0)	// SysTick CTRL: ENABLE Mask
#define SysTick_CTRL_DISABLE	(0 << 0)	// SysTick CTRL: ENABLE Mask

typedef struct {
  int CTRL;							// Offset: 0x000 (R/W)  SysTick Control and Status Register
  int LOAD;							// Offset: 0x004 (R/W)  SysTick Reload Value Register
  int VAL;							// Offset: 0x008 (R/W)  SysTick Current Value Register
  int CALIB;						// Offset: 0x00C (R/ )  SysTick Calibration Register
} SysTick_T;

/************************************************************************************
 *	UART / USART																	*
 ************************************************************************************/
#define USART0_BASE					0x40081000
#define USART2_BASE0				0x400C1000
#define USART3_BASE					0x400C2000
#define UART1_BASE					0x40082000

#define USART0						((USART_T *) LPC_USART0_BASE)
#define USART2						((USART_T *) LPC_USART2_BASE)
#define USART3						((USART_T *) LPC_USART3_BASE)
#define UART1						((USART_T *) LPC_UART1_BASE)

/************************************************************************************
 *	NVIC																			*
 ************************************************************************************/
#define _NVIC_BASE					0xE000E100		// NVIC Base Address (Tabla 81, pag. 115)
#define _NVIC						((NVIC_T *)  _NVIC_BASE)

typedef struct {
	int ISER[8];					// Offset: 0x000 (R/W)  Interrupt Set Enable Register
	int RESERVED0[24];
	int ICER[8];					// Offset: 0x080 (R/W)  Interrupt Clear Enable Register
	int RSERVED1[24];
	int ISPR[8];					// Offset: 0x100 (R/W)  Interrupt Set Pending Register
	int RESERVED2[24];
	int ICPR[8];					// Offset: 0x180 (R/W)  Interrupt Clear Pending Register
	int RESERVED3[24];
	int IABR[8];					// Offset: 0x200 (R/W)  Interrupt Active bit Register
	int RESERVED4[56];
	unsigned char IP[240];			// Offset: 0x300 (R/W)  Interrupt Priority Register (8Bit wide)
    int RESERVED5[644];
	int STIR;						// Offset: 0xE00 ( /W)  Software Trigger Interrupt Register
} NVIC_T;

#ifdef NO_LIBS
typedef enum {
	/* -------------------------  Cortex-M4 Processor Exceptions Numbers  ----------------------------- */
	Reset_IRQn                = -15, //  1  Reset Vector, invoked on Power up and warm reset
	NonMaskableInt_IRQn       = -14, //  2  Non maskable Interrupt, cannot be stopped or preempted
	HardFault_IRQn            = -13, //  3  Hard Fault, all classes of Fault
	MemoryManagement_IRQn     = -12, //  4  Memory Management, MPU mismatch, including Access Violation and No Match
	BusFault_IRQn             = -11, //  5  Bus Fault, Pre-Fetch-, Memory Access Fault, other address/memory related Fault
	UsageFault_IRQn           = -10, //  6  Usage Fault, i.e. Undef Instruction, Illegal State Transition
	SVCall_IRQn               =  -5, // 11  System Service Call via SVC instruction
	DebugMonitor_IRQn         =  -4, // 12  Debug Monitor                    
	PendSV_IRQn               =  -2, // 14  Pendable request for system service
	SysTick_IRQn              =  -1, // 15  System Tick Timer

	/* ----------------------  Numero de interrupciones especificas del LPC4337  ---------------------- */
	DAC_IRQn                  =   0, //  0  DAC                              
	M0APP_IRQn                =   1, //  1  M0APP Core interrupt             
	DMA_IRQn                  =   2, //  2  DMA                              
	RESERVED1_IRQn            =   3, //  3  EZH/EDM                          
	RESERVED2_IRQn            =   4,
	ETHERNET_IRQn             =   5, //  5  ETHERNET                         
	SDIO_IRQn                 =   6, //  6  SDIO                             
	LCD_IRQn                  =   7, //  7  LCD                              
	USB0_IRQn                 =   8, //  8  USB0                             
	USB1_IRQn                 =   9, //  9  USB1                             
	SCT_IRQn                  =  10, // 10  SCT                              
	RITIMER_IRQn              =  11, // 11  RITIMER                          
	TIMER0_IRQn               =  12, // 12  TIMER0                           
	TIMER1_IRQn               =  13, // 13  TIMER1                           
	TIMER2_IRQn               =  14, // 14  TIMER2                           
	TIMER3_IRQn               =  15, // 15  TIMER3                           
	MCPWM_IRQn                =  16, // 16  MCPWM                            
	ADC0_IRQn                 =  17, // 17  ADC0                             
	I2C0_IRQn                 =  18, // 18  I2C0                             
	I2C1_IRQn                 =  19, // 19  I2C1                             
	SPI_INT_IRQn              =  20, // 20  SPI_INT                          
	ADC1_IRQn                 =  21, // 21  ADC1                             
	SSP0_IRQn                 =  22, // 22  SSP0                             
	SSP1_IRQn                 =  23, // 23  SSP1                             
	USART0_IRQn               =  24, // 24  USART0                           
	UART1_IRQn                =  25, // 25  UART1                            
	USART2_IRQn               =  26, // 26  USART2                           
	USART3_IRQn               =  27, // 27  USART3                           
	I2S0_IRQn                 =  28, // 28  I2S0                             
	I2S1_IRQn                 =  29, // 29  I2S1                             
	RESERVED4_IRQn            =  30,
	SGPIO_INT_IRQn            =  31, // 31  SGPIO_IINT                       
	PIN_INT0_IRQn             =  32, // 32  PIN_INT0                         
	PIN_INT1_IRQn             =  33, // 33  PIN_INT1                         
	PIN_INT2_IRQn             =  34, // 34  PIN_INT2                         
	PIN_INT3_IRQn             =  35, // 35  PIN_INT3                         
	PIN_INT4_IRQn             =  36, // 36  PIN_INT4                         
	PIN_INT5_IRQn             =  37, // 37  PIN_INT5                         
	PIN_INT6_IRQn             =  38, // 38  PIN_INT6                         
	PIN_INT7_IRQn             =  39, // 39  PIN_INT7                         
	GINT0_IRQn                =  40, // 40  GINT0                            
	GINT1_IRQn                =  41, // 41  GINT1                            
	EVENTROUTER_IRQn          =  42, // 42  EVENTROUTER                      
	C_CAN1_IRQn               =  43, // 43  C_CAN1                           
	RESERVED6_IRQn            =  44,
	ADCHS_IRQn                =  45, // 45  ADCHS interrupt                  
	ATIMER_IRQn               =  46, // 46  ATIMER                           
	RTC_IRQn                  =  47, // 47  RTC                              
	RESERVED8_IRQn            =  48,
	WWDT_IRQn                 =  49, // 49  WWDT                             
	M0SUB_IRQn                =  50, // 50  M0SUB core interrupt             
	C_CAN0_IRQn               =  51, // 51  C_CAN0                           
	QEI_IRQn                  =  52, // 52  QEI                              
} IRQn_Type;
#endif

/************************************************************************************
 *	DAC																				*
 ************************************************************************************/
#define _DAC_BASE				0x400E1000	// DAC Base Address
#define _DAC					((DAC_T *) 	_DAC_BASE )

#define DAC_BIAS_1MHz	((unsigned int) (0 << 16))
#define DAC_BIAS_400kHz ((unsigned int) (1 << 16))

// D/A Control register
#define INT_DMA_REQ	(1 << 0)	//DMA request
#define DBLBUF_ENA	(1 << 1)	//DMA double-buffering
#define CNT_ENA		(1 << 2)	//DMA time-out
#define DMA_ENA		(1 << 3)	//Combined DAC enable and DMA enable

typedef struct {				// DAC Structure
	int CR;						// DAC register. Holds the conversion data.
	int CTRL;					// DAC control register
	int CNTVAL;					// DAC counter value register
} DAC_T;

/************************************************************************************
 *	DMA																			*
 ************************************************************************************/
#define _GPDMA_BASE  	0x40002000
#define _GPDMA			((GPDMA_T*) _GPDMA_BASE)

#define LLI_CTRL_MASK 	(WAVE_SAMPLE_NUM | (0 << 12) | (0 << 15) | (2 << 18) | (2 << 21) | (0 << 24) | (1 << 25) | (1 << 26) | (0 << 27) | (0 << 28) | (0 << 29) | (0 << 30) | (1 << 31))
#define GPDMA_NUMBER_CHANNELS 8
/* WAVE_SAMPLE_NUM // transfer size (0 - 11) = 64
| (0 << 12)  	// source burst size (12 - 14) = 1
| (0 << 15)  	// destination burst size (15 - 17) = 1
| (2 << 18)  	// source width (18 - 20) = 32 bit
| (2 << 21)  	// destination width (21 - 23) = 32 bit
| (0 << 24)  	// source AHB select (24) = AHB 0
| (1 << 25)  	// destination AHB select (25) = AHB 1
| (1 << 26)  	// source increment (26) = increment
| (0 << 27)  	// destination increment (27) = no increment
| (0 << 28)  	// mode select (28) = access in user mode
| (0 << 29)  	// (29) = access not bufferable
| (0 << 30)  	// (30) = access not cacheable
| (0 << 31) 	// terminal count interrupt disabled (deshabilita (0) / habilita (1)) */

struct LLI_T {
	unsigned int  	SRC;				// DMA Channel Source Address Register
	unsigned int  	DEST;				// DMA Channel Destination Address Register
	unsigned int  	NEXT;				// DMA Channel Linked List Item Register
	unsigned int  	CONTROL;			// DMA Channel Control Register
};

typedef struct {
	struct LLI_T  	LLI;			// DMA Channel Linked List Item Register
	unsigned int  	CONFIG;			// DMA Channel Configuration Register
	unsigned int  	RESERVED1[3];
} _GPDMA_CH_T;

typedef struct {					// GPDMA Structure
	unsigned int  	INTSTAT;		// DMA Interrupt Status Register
	unsigned int  	INTTCSTAT;		// DMA Interrupt Terminal Count Request Status Register
	unsigned int  	INTTCCLEAR;		// DMA Interrupt Terminal Count Request Clear Register
	unsigned int  	INTERRSTAT;		// DMA Interrupt Error Status Register
	unsigned int  	INTERRCLR;		// DMA Interrupt Error Clear Register
	unsigned int  	RAWINTTCSTAT;	// DMA Raw Interrupt Terminal Count Status Register
	unsigned int  	RAWINTERRSTAT;	// DMA Raw Error Interrupt Status Register
	unsigned int  	ENBLDCHNS;		// DMA Enabled Channel Register
	unsigned int  	SOFTBREQ;		// DMA Software Burst Request Register
	unsigned int  	SOFTSREQ;		// DMA Software Single Request Register 
	unsigned int  	SOFTLBREQ;		// DMA Software Last Burst Request Register
	unsigned int  	SOFTLSREQ;		// DMA Software Last Single Request Register
	unsigned int  	CONFIG;			// DMA Configuration Register
	unsigned int  	SYNC;				// DMA Synchronization Register
	unsigned int  	RESERVED0[50];
	_GPDMA_CH_T   	CH[GPDMA_NUMBER_CHANNELS];
} _GPDMA_T;

// USART Register
typedef struct {

	union {
		unsigned int  DLL; 		// Divisor Latch LSB. Least significant byte of the baud rate divisor value. The full divisor is used to generate a baud rate from the fractional rate divider (DLAB = 1).
		unsigned int  THR; 		// Transmit Holding Register. The next character to be transmitted is written here (DLAB = 0).
		unsigned int  RBR; 		// Receiver Buffer Register. Contains the next received character to be read (DLAB = 0).
	};

	union {
		unsigned int IER;		// Interrupt Enable Register. Contains individual interrupt enable bits for the 7 potential UART interrupts (DLAB = 0).
		unsigned int DLM;		// Divisor Latch MSB. Most significant byte of the baud rate divisor value. The full divisor is used to generate a baud rate from the fractional rate divider (DLAB = 1).
	};

	union {
		unsigned int FCR;		// FIFO Control Register. Controls UART FIFO usage and modes.
		unsigned int IIR;		// Interrupt ID Register. Identifies which interrupt(s) are pending.
	};

	unsigned int LCR;			// Line Control Register. Contains controls for frame formatting and break generation.
	unsigned int MCR;			// Modem Control Register. Only present on USART ports with full modem support.
	unsigned int LSR;			// Line Status Register. Contains flags for transmit and receive status, including line errors.
	unsigned int MSR;			// Modem Status Register. Only present on USART ports with full modem support.
	unsigned int SCR;			// Scratch Pad Register. Eight-bit temporary storage for software.
	unsigned int ACR;			// Auto-baud Control Register. Contains controls for the auto-baud feature.
	unsigned int ICR;			// IrDA control register (not all UARTS)
	unsigned int FDR;			// Fractional Divider Register. Generates a clock input for the baud rate divider.
	unsigned int OSR;			// Oversampling Register. Controls the degree of oversampling during each bit time. Only on some UARTS.
	unsigned int TER1;			// Transmit Enable Register. Turns off USART transmitter for use with software flow control.
	unsigned int RESERVED0[3];
    unsigned int HDEN;			// Half-duplex enable Register- only on some UARTs
	unsigned int RESERVED1[1];
	unsigned int SCICTRL;		// Smart card interface control register- only on some UARTs

	unsigned int RS485CTRL;		// RS-485/EIA-485 Control. Contains controls to configure various aspects of RS-485/EIA-485 modes.
	unsigned int RS485ADRMATCH;	// RS-485/EIA-485 address match. Contains the address match value for RS-485/EIA-485 mode.
	unsigned int RS485DLY;		// RS-485/EIA-485 direction control delay.

	union {
		unsigned int SYNCCTRL;	// Synchronous mode control register. Only on USARTs.
		unsigned int FIFOLVL;	// FIFO Level register. Provides the current fill levels of the transmit and receive FIFOs.
	};

	unsigned int TER2;			// Transmit Enable Register. Only on LPC177X_8X UART4 and LPC18XX/43XX USART0/2/3.
} _USART_T;

/************************************************************************************
 *	ADC0/1																			*
 ************************************************************************************/
#define _ADC0_BASE		0x400E3000
#define ADC0			((_ADC_T*) _ADC0_BASE)

#define _ADC1_BASE		0x400E4000
#define ADC1			((_ADC_T*) _ADC1_BASE)

#define CR_START_MODE_SEL(SEL)  ((SEL << 24))
#define DR_RESULT(n)        	((((n) >> 6) & 0x3FF))
#define DR_DONE(n)          	(((n) >> 31))
#define CR_PDN              	((1 << 21))
#define SAMP_RATE_CFG_MASK      (CR_CLKDIV(0xFF) | CR_BITACC(0x07))
#define CR_BURST            	((1 << 16))
#define CR_CLKDIV(n)        	((((n) & 0xFF) << 8))
#define CR_BITACC(n)        	((((n) & 0x7) << 17))
#define CR_CH_SEL(n)        	((1 << (n)))

typedef struct {			// ADC Structure
	uint32_t CR;			// A/D Control Register. The AD0CR register must be written to select the operating mode before A/D conversion can occur. 
	uint32_t GDR;			// A/D Global Data Register. Contains the result of the most recent A/D conversion. 
	uint32_t RESERVED0;
	uint32_t INTEN;			// A/D Interrupt Enable Register. This register contains enable bits that allow the DONE flag of each A/D channel to be included or excluded from contributing to the generation of an A/D interrupt.
	uint32_t DR[8];			// A/D Channel Data Register. This register contains the result of the most recent conversion completed on channel n. 
	uint32_t STAT;			// A/D Status Register. This register contains DONE and OVERRUN flags for all of the A/D channels, as well as the A/D interrupt flag.
} _ADC_T;

typedef struct {
	uint32_t adcRate;		// ADC rate
	uint8_t  bitsAccuracy;	// ADC bit accuracy
	bool     burstMode;		// ADC Burt Mode 
} _ADC_CLOCK_SETUP_T;

typedef enum _ADC_RESOLUTION {
	R10BITS = 0,		// ADC 10 bits
	R9BITS,			// ADC 9 bits
	R8BITS,			// ADC 8 bits
	R7BITS,			// ADC 7 bits
	R6BITS,			// ADC 6 bits
	R5BITS,			// ADC 5 bits
	R4BITS,			// ADC 4 bits
	R3BITS,			// ADC 3 bits
} _ADC_RESOLUTION_T;

typedef enum _ADC_CHANNEL {
	CH0 = 0,		// ADC channel 0 
	CH1,			// ADC channel 1 
	CH2,			// ADC channel 2 
	CH3,			// ADC channel 3 
	CH4,			// ADC channel 4 
	CH5,			// ADC channel 5
	CH6,			// ADC channel 6 
	CH7,			// ADC channel 7 
} _ADC_CHANNEL_T;

typedef enum _ADC_STATUS {
	DR_DONE_STAT,	// ADC data register staus
	DR_OVERRUN_STAT,// ADC data overrun staus
	DR_ADINT_STAT	// ADC interrupt status
} _ADC_STATUS_T;

typedef enum _ADC_EDGE_CFG {
	TRIGGERMODE_RISING = 0,	// Trigger event: rising edge 
	TRIGGERMODE_FALLING,	// Trigger event: falling edge
} _ADC_EDGE_CFG_T;

typedef enum _ADC_START_MODE {
	NO_START = 0,
	START_NOW,			// Start conversion now 
	START_ON_CTOUT15,	// Start conversion when the edge selected by bit 27 occurs on CTOUT_15
	START_ON_CTOUT8,	// Start conversion when the edge selected by bit 27 occurs on CTOUT_8
	START_ON_ADCTRIG0,	// Start conversion when the edge selected by bit 27 occurs on ADCTRIG0
	START_ON_ADCTRIG1,	// Start conversion when the edge selected by bit 27 occurs on ADCTRIG1 
	START_ON_MCOA2		// Start conversion when the edge selected by bit 27 occurs on Motocon PWM output MCOA2 
} _ADC_START_MODE_T;

void GPIO_Init(_GPIO_T *pGPIO);
void GPIO_SetPinDIROutput(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin);
void GPIO_SetPin(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin, int valor);
void GPIO_ClearPin(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin, int valor);
void GPIO_NOTPin(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin);
int GPIO_GetPinState(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin);
void GPIO_SetPinDIRInput(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin);
int GPIO_GetPinDIR(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin);
void GPIO_SetPortDIROutput(_GPIO_T *pGPIO, unsigned char puerto, int mascara);
void GPIO_SetPortDIRInput(_GPIO_T *pGPIO, unsigned char puerto, int mascara);
int GPIO_GetPortDIR(_GPIO_T *pGPIO, unsigned char puerto);
void SCU_PinMux(uint8_t puerto, uint8_t pin, uint16_t func);
void GPIO_EscribirPin(_GPIO_T *pGPIO, uint8_t puerto, uint8_t pin, bool valor);

void Config_LEDS(int MASK);
void Config_Botones(int MASK);

void LED_ON(enum LEDS);
void LED_OFF(enum LEDS);
void NOT_LED(enum LEDS);

void SysTick_Conf(uint32_t ticks);

void UART_Init(void);

void SCU_GPIOIntPinSel(unsigned char PortSel, unsigned char PortNum, unsigned char PinNum);

void NVIC_SetPri(IRQn_Type IRQn, unsigned int priority);
void NVIC_EnaIRQ(IRQn_Type IRQn);
void NVIC_ClrPendIRQ(IRQn_Type IRQn);
void PININT_Init(PIN_INT_T *pPININT);
void PININT_Edge(PIN_INT_T *pPININT, uint32_t pines);
void PININT_Low(PIN_INT_T *pPININT, uint32_t pines);
void PININT_High(PIN_INT_T *pPININT, uint32_t pines);
void PININT_ClearInt(PIN_INT_T *pPININT, uint32_t pines);
void ADC_Habilitar(_ADC_T *pADC, _ADC_START_MODE_T modo, _ADC_EDGE_CFG_T flanco);
bool ADC_Leer(_ADC_T *pADC, uint8_t canal, uint16_t *data);
bool ADC_Estado(_ADC_T *pADC, uint8_t canal);
void ADC_Canal(_ADC_T *pADC, uint8_t canal, bool estado);
void ADC_Init(_ADC_T *pADC, _ADC_CLOCK_SETUP_T *ADCSetup, _ADC_CHANNEL_T canal, uint32_t rate, _ADC_RESOLUTION_T resolucion, bool burst);