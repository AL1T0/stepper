/**********************************************************************************************
 * Trabajo práctico integrador de la aignatura Electrónica Digital II
 * de la Universidad Nacional de San Martín (UNSAM)
 *
 * Documentacion:
 *    - UM10503 (LPC43xx ARM Cortex-M4/M0 multi-core microcontroller User Manual)
 *    - PINES UTILIZADOS DEL NXP LPC4337 JBD144 (Ing. Eric Pernia)
 **********************************************************************************************/

#include "funciones.h"

//***************************************************************************************************
void GPIO_Init(_GPIO_T *pGPIO) {}
//***************************************************************************************************
void GPIO_SetPinDIROutput(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin) {
	pGPIO->DIR[puerto]|=(1<<pin);
}
//***************************************************************************************************
void GPIO_SetPin(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin, int valor) {
	pGPIO->SET[puerto]|=(valor<<pin);
}
//***************************************************************************************************
void GPIO_ClearPin(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin, int valor) {
	pGPIO->CLR[puerto]|=(valor<<pin);
}
//***************************************************************************************************
void GPIO_NOTPin(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin){
	pGPIO->NOT[puerto] = 1 << pin ;	
}
//***************************************************************************************************
int GPIO_GetPinState(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin) {
	int state;
	state = pGPIO->PIN[puerto];
	(state >> pin) && 0x1;
	return(state);
}
//***************************************************************************************************
void GPIO_SetPinDIRInput(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin) {
	pGPIO->DIR[puerto]|=(0<<pin);
}
//***************************************************************************************************
int GPIO_GetPinDIR(_GPIO_T *pGPIO, unsigned char puerto, unsigned char pin) {
	int state;
	state = pGPIO->DIR[puerto];
	(state >> pin) && 0x1;
	return(state);
} 
//***************************************************************************************************
void GPIO_SetPortDIROutput(_GPIO_T *pGPIO, unsigned char puerto, int mascara) {
	pGPIO->DIR[puerto] |= mascara;
}
//***************************************************************************************************
void GPIO_SetPortDIRInput(_GPIO_T *pGPIO, unsigned char puerto, int mascara) {
	pGPIO->DIR[puerto] &= ~(mascara);
}
//***************************************************************************************************
int GPIO_GetPortDIR(_GPIO_T *pGPIO, unsigned char puerto){
	int state;
	state = pGPIO->DIR[puerto];
	return(state);
}
//***************************************************************************************************
void GPIO_EscribirPin(_GPIO_T *pGPIO, uint8_t puerto, uint8_t pin, bool valor)
{
	pGPIO->B[puerto][pin] = valor;
}
//***************************************************************************************************
void SCU_PinMux(uint8_t puerto, uint8_t pin, uint16_t func)
{
	_SCU->SFSP[puerto][pin] = func;
}
//***************************************************************************************************
void LED_ON(enum LEDS led) {
	
	switch(led) {
		case 0:
			_GPIO_PORT->SET[0] |= (0 << 14);
			break;
		case 1:
			_GPIO_PORT->SET[1] |= (1 << 11);
			break;
		case 2:
			_GPIO_PORT->SET[1] |= 1 << 12;
			break;
		case 3:
			_GPIO_PORT->SET[5] |= 1 << 0;
			break;
		case 4:
			_GPIO_PORT->SET[5] |= 1 << 1;
			break;
		case 5:
			_GPIO_PORT->SET[5] |= 1 << 2;
			break;
	}
}
//***************************************************************************************************
void LED_OFF(enum LEDS led) {
	
	switch(led) {
		case 0:
			_GPIO_PORT->CLR[0] |= (1 << 14);
			break;
		case 1:
			_GPIO_PORT->CLR[1] |= (1 << 11);
			break;
		case 2:
			_GPIO_PORT->CLR[1] |= 1 << 12;
			break;
		case 3:
			_GPIO_PORT->CLR[5] |= 1 << 0;
			break;
		case 4:
			_GPIO_PORT->CLR[5] |= 1 << 1;
			break;
		case 5:
			_GPIO_PORT->CLR[5] |= 1 << 2;
			break;
	}
}
//***************************************************************************************************
void NOT_LED(enum LEDS led) {
	
	switch(led) {
		case 0:
			GPIO_NOTPin(_GPIO_PORT,0,14);
			break;
		case 1:
			GPIO_NOTPin(_GPIO_PORT,1,11);
			break;
		case 2:
			GPIO_NOTPin(_GPIO_PORT,1,12);
			break;
		case 3:
			GPIO_NOTPin(_GPIO_PORT,5,0);
			break;
		case 4:
			GPIO_NOTPin(_GPIO_PORT,5,1);
			break;
		case 5:
			GPIO_NOTPin(_GPIO_PORT,5,2);
			break;
	}
}
//***************************************************************************************************
void Config_LEDS(int MASK) {
	// Configuracion de los pines (LED1, LED2, LED3, LEDR, LEDG y LEDB) como GPIO
	// (Registro de configuracion, pag 405 / Tabla 191)
	_SCU->SFSP[2][10] = (MASK | SCU_MODE_FUNC0); // P2_10, GPIO0[14], LED1
	_SCU->SFSP[2][11] = (MASK | SCU_MODE_FUNC0); // P2_11, GPIO1[11], LED2
	_SCU->SFSP[2][12] = (MASK | SCU_MODE_FUNC0); // P2_12, GPIO1[12], LED3
	_SCU->SFSP[2][0] = (MASK | SCU_MODE_FUNC4); 	// P2_0,  GPIO5[0],  LEDR
	_SCU->SFSP[2][1] = (MASK | SCU_MODE_FUNC4); 	// P2_1,  GPIO5[1],  LEDG
	_SCU->SFSP[2][2] = (MASK | SCU_MODE_FUNC4); 	// P2_2,  GPIO5[2],  LEDB
	
	// Configuracion de los pines (LED1, LED2, LED3, LEDR, LEDG y LEDB) como salida
	// (Registro de direccion, pag 455 -> Tabla 261)
	GPIO_SetPinDIROutput(_GPIO_PORT, 0, 14);
	GPIO_SetPinDIROutput(_GPIO_PORT, 1, 11);
	GPIO_SetPinDIROutput(_GPIO_PORT, 1, 12);
	GPIO_SetPinDIROutput(_GPIO_PORT, 5, 0);
	GPIO_SetPinDIROutput(_GPIO_PORT, 5, 1);
	GPIO_SetPinDIROutput(_GPIO_PORT, 5, 2);
	
}
//***************************************************************************************************
void Config_Botones(int MASK) {
	_SCU->SFSP[1][0] = (MASK | SCU_MODE_FUNC0); 	// P1_0, GPIO0[4], TEC_1
	_SCU->SFSP[1][1] = (MASK | SCU_MODE_FUNC0); 	// P1_1, GPIO0[8], TEC_2
	_SCU->SFSP[1][2] = (MASK | SCU_MODE_FUNC0); 	// P1_2, GPIO0[9], TEC_3
	_SCU->SFSP[1][6] = (MASK | SCU_MODE_FUNC0);	// P1_6, GPIO1[9], TEC_4

	GPIO_SetPinDIRInput(_GPIO_PORT, 0, 4);
	GPIO_SetPinDIRInput(_GPIO_PORT, 0, 8);
	GPIO_SetPinDIRInput(_GPIO_PORT, 0, 9);
	GPIO_SetPinDIRInput(_GPIO_PORT, 1, 9);
	
}
//***************************************************************************************************
void SysTick_Conf(uint32_t ticks) {
    _SysTick->CTRL = 0;
    _SysTick->LOAD = ticks - 1;  // _SysTick->CALIB; 
	_SysTick->VAL = 0;    	     // Clear current value as well as count flag
	_SysTick->CTRL |= (SysTick_CTRL_ENABLE | SysTick_CTRL_TICKINT | SysTick_CTRL_CLKSOURCE);
    NVIC_SetPri(SysTick_IRQn, (1<<3) - 1);
}
//***************************************************************************************************
void UART_Init(void){

   //Initialize peripheral
   Chip_UART_Init(CIAA_BOARD_UART);
	
   // Set Baud rate
   Chip_UART_SetBaud(CIAA_BOARD_UART, SYSTEM_BAUD_RATE);

   //Modify FCR (FIFO Control Register)
   Chip_UART_SetupFIFOS(CIAA_BOARD_UART, UART_FCR_FIFO_EN | UART_FCR_TRG_LEV0);

   // Enable UART Transmission
   Chip_UART_TXEnable(CIAA_BOARD_UART);

   Chip_SCU_PinMux(7, 1, MD_PDN, FUNC6);              /* P7_1: UART2_TXD */
   Chip_SCU_PinMux(7, 2, MD_PLN|MD_EZI|MD_ZI, FUNC6); /* P7_2: UART2_RXD */

   //Enable UART Rx Interrupt
   Chip_UART_IntEnable(CIAA_BOARD_UART,UART_IER_RBRINT);   //Receiver Buffer Register Interrupt
   
   // Enable UART line status interrupt
   //Chip_UART_IntEnable(CIAA_BOARD_UART,UART_IER_RLSINT ); //LPC43xx User manual page 1118
   NVIC_SetPriority(USART2_IRQn, 6);
   
   // Enable Interrupt for UART channel
//   NVIC_EnableIRQ(USART2_IRQn);
//	NVIC_EnaIRQ(USART2_IRQn);
}

/************************************************************************
 * GPIO Interrupt Pin Select
 * PortSel	: Numero de interrupcion de GPIO (0 a 7)
 * PortNum	: GPIO port number interrupt, should be: 0 to 7
 * PinNum	: GPIO pin number Interrupt , should be: 0 to 31
 ************************************************************************/
void SCU_GPIOIntPinSel(unsigned char PortSel, unsigned char PortNum, unsigned char PinNum){
	int despl = (PortSel & 3) << 3;
	unsigned int val = (((PortNum & 0x7) << 5) | (PinNum & 0x1F)) << despl;
	_SCU->PINTSEL[PortSel >> 2] = (_SCU->PINTSEL[PortSel >> 2] & ~(0xFF << despl)) | val;
}

/************************************************************************
 * Establecimiento de la prioridad de una interrupcion
 ************************************************************************/
void NVIC_SetPri(IRQn_Type IRQn, unsigned int prioridad){
	if(IRQn < 0) {
	}
	else {
		_NVIC->IP[(unsigned int)(IRQn)] = ((prioridad << (8 - 2)) & 0xff);
	}
}
//***************************************************************************************************
void NVIC_EnaIRQ(IRQn_Type IRQn){
    _NVIC->ISER[(unsigned int)((int)IRQn) >> 5] = (unsigned int)(1 << ((unsigned int)((int32_t)IRQn) & (unsigned int)0x1F));
}
//***************************************************************************************************
void NVIC_ClrPendIRQ(IRQn_Type IRQn)
{
    _NVIC->ICPR[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F)); /* Clear pending interrupt */
}
//***************************************************************************************************
void PININT_Init(PIN_INT_T *pPININT) {}
//***************************************************************************************************
void PININT_Edge(PIN_INT_T *pPININT, uint32_t pines)
{
	pPININT->ISEL &= ~pines;
}
//***************************************************************************************************
void PININT_Low(PIN_INT_T *pPININT, uint32_t pines)
{
	pPININT->SIENF = pines;
}
//***************************************************************************************************
void PININT_High(PIN_INT_T *pPININT, uint32_t pines)
{
	pPININT->SIENR = pines;
}
//***************************************************************************************************
void PININT_ClearInt(PIN_INT_T *pPININT, uint32_t pines)
{
	pPININT->IST = pines;
}
//***************************************************************************************************
void ADC_Canal(_ADC_T *pADC, uint8_t canal, bool estado)
{
	if (estado == 1) {
		pADC->INTEN |= (1 << canal);
	}
	else {
		pADC->INTEN &= (~(1 << canal));
	}
}
//***************************************************************************************************
void ADC_Habilitar(_ADC_T *pADC, _ADC_START_MODE_T modo, _ADC_EDGE_CFG_T flanco)
{
	uint32_t temp;
	if ((modo != START_NOW) && (modo != NO_START)) {
		if (flanco) {
			pADC->CR |= (1 << 27);
		}
		else {
			pADC->CR &= ~(1 << 27);
		}
	}
	
	temp = pADC->CR & (~(7 << 24));
	pADC->CR = temp | CR_START_MODE_SEL(modo);
}
//***************************************************************************************************
bool ADC_Leer(_ADC_T *pADC, uint8_t canal, uint16_t *data)
{
	if (!DR_DONE(pADC->DR[canal])) {
	return 0;
	}
	*data = (uint16_t) DR_RESULT(pADC->DR[canal]);
	return 1;
}
//***************************************************************************************************
bool ADC_Estado(_ADC_T *pADC, uint8_t canal)
{
	if (pADC->STAT & (1 << canal)){
		return 1;
	}
	return 0;
}
//***************************************************************************************************
void ADC_Init(_ADC_T *pADC, _ADC_CLOCK_SETUP_T *ADCSetup, _ADC_CHANNEL_T canal, uint32_t rate, _ADC_RESOLUTION_T resolucion, bool burst)
{
	uint32_t cr = 0;

//	Chip_Clock_EnableOpts(Chip_ADC_GetClockIndex(pADC), true, true, 1);

	pADC->INTEN = 0; // Deshabilitar toadas las interrupciones

	cr |= CR_PDN;
	ADCSetup->adcRate = rate;
	ADCSetup->bitsAccuracy = resolucion;
	cr |= CR_CLKDIV(80);
	cr |= CR_BITACC(ADCSetup->bitsAccuracy);
	cr |= CR_CH_SEL(canal);
	pADC->CR = cr;
	
	// Burst mode	
    if (burst == 0) {
		pADC->CR &= ~CR_BURST;
	}
	else {
		pADC->CR |= CR_BURST;
	}
}